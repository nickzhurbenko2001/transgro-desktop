﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kursa4Trasport
{
    public partial class Form1 : Form
    {
        public string ConnStr = "Data Source=DESKTOP-SJBK3TL\\TRANSBD;Initial Catalog = TransGro; User ID = DBC; Password=12345";
        public Form1()
        {
            InitializeComponent();
        }

        private void avtobutton_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(ConnStr);
            conn.Open();
            this.Hide();
            AvtoForm avtoForm = new AvtoForm();
            avtoForm.Show();
        }

        private void trollbutton_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(ConnStr);
            conn.Open();
            this.Hide();
            trollForm trollForm = new trollForm();
            trollForm.Show();
        }

        private void ostbutton_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(ConnStr);
            conn.Open();
            this.Hide();
            ostForm ostForm = new ostForm();
            ostForm.Show();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.Hide();
            AvtoForm avto = new AvtoForm();
            avto.Show();
            
        }
    }
}
