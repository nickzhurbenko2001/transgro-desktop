﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kursa4Trasport
{
    public partial class Choose : Form
    {
        public string ost;
        public string text;
        public string text1;
     
        DBconnect dBconnect = new DBconnect();
        public Choose()
        {
            InitializeComponent();
            button2.Enabled = false;
            button3.Enabled = false;

        }
       
        private void Choose_Load(object sender, EventArgs e)
        {

            

            label1.Text = text;
            label2.Text = text1;
            dBconnect.ConnOpen();
            SqlCommand command = new SqlCommand("SELECT Ostanovka From Ostanovki WHERE Marshrut = @marsh", dBconnect.SqlConnection());
            command.Parameters.Add("@marsh", SqlDbType.VarChar).Value = text;

            SqlDataReader read = command.ExecuteReader();
            while (read.Read())
            {
                listBox1.Items.Add($"{read["Ostanovka"]}");
            }
            read.Close();

            SqlCommand command1 = new SqlCommand("SELECT Ostanovka From Ostanovki WHERE Marshrut = @marsh", dBconnect.SqlConnection());
            command1.Parameters.Add("@marsh", SqlDbType.VarChar).Value = text1;

            SqlDataReader read1 = command1.ExecuteReader();
            while (read1.Read())
            {
                listBox2.Items.Add($"{read1["Ostanovka"]}");
            }
            read1.Close();


        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            AvtoForm avtoForm = new AvtoForm();
            avtoForm.Show();
            this.Close();
          
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
         
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            ost = listBox1.SelectedItem.ToString();

            //try
            //{

            //    listBox1.SelectedItem = null;
            //}
            //catch
            //{
            //    MessageBox.Show("Выберите остановку!");
            //}
            Time time = new Time();
            time.ostname = ost;
            time.Marsh = text;
            this.Hide();
            time.Show();
          

        }

        private void button3_Click(object sender, EventArgs e)
        {
            ost = listBox2.SelectedItem.ToString();
          
            Time time = new Time();
            time.ostname = ost;
            time.Marsh = text1;
            this.Hide();
            time.Show();
            
        }

        private void listBox1_Click(object sender, EventArgs e)
        {
            button2.Enabled = true;
        }

        private void listBox2_Click(object sender, EventArgs e)
        {
            button3.Enabled = true;
        }
    }
}
