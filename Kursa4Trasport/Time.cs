﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Sql;


namespace Kursa4Trasport
{
    public partial class Time : Form
    {
      
        public string ostname;
        public string Marsh;
        public string num;
        
        public Time()
        {
           
            InitializeComponent();
            button1.Visible = false;
            
        }

        private void Time_Load(object sender, EventArgs e)
        {
            string DBchoose = ostname+ Marsh;
            label1.Text = ostname;
            label2.Text = Marsh;
            label3.Text = "Остановка:";
            label5.Text = num;


            
            DBconnect dBconnect = new DBconnect();
            dBconnect.ConnOpen();
            SqlCommand comm = new SqlCommand($"SELECT * FROM [{DBchoose}]", dBconnect.SqlConnection());
            comm.Parameters.Add("@table", SqlDbType.VarChar).Value = DBchoose;
           
           
            SqlDataReader reader = comm.ExecuteReader();
            List<string[]> data = new List<string[]>(); 
            while(reader.Read())
            {
                data.Add(new string[24]);

                data[data.Count - 1][0] = reader[0].ToString();
                data[data.Count - 1][1] = reader[1].ToString();

                

            }
            reader.Close();


            foreach (string[] s in data)
            {
                DataView.Rows.Add(s);
            }
            
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click_1(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
            AvtoForm avto = new AvtoForm();
            avto.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //this.Close();
            //Form1 mainM = new Form1();
            //mainM.Show();
        }
    }
}
