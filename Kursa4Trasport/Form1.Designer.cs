﻿namespace Kursa4Trasport
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.avtobutton = new System.Windows.Forms.Button();
            this.trollbutton = new System.Windows.Forms.Button();
            this.ostbutton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // avtobutton
            // 
            this.avtobutton.AutoSize = true;
            this.avtobutton.Image = ((System.Drawing.Image)(resources.GetObject("avtobutton.Image")));
            this.avtobutton.Location = new System.Drawing.Point(69, 36);
            this.avtobutton.Name = "avtobutton";
            this.avtobutton.Size = new System.Drawing.Size(356, 356);
            this.avtobutton.TabIndex = 0;
            this.avtobutton.UseVisualStyleBackColor = true;
            this.avtobutton.Click += new System.EventHandler(this.avtobutton_Click);
            // 
            // trollbutton
            // 
            this.trollbutton.Location = new System.Drawing.Point(622, 12);
            this.trollbutton.Name = "trollbutton";
            this.trollbutton.Size = new System.Drawing.Size(75, 75);
            this.trollbutton.TabIndex = 1;
            this.trollbutton.Text = "Троллейбусы";
            this.trollbutton.UseVisualStyleBackColor = true;
            this.trollbutton.Visible = false;
            this.trollbutton.Click += new System.EventHandler(this.trollbutton_Click);
            // 
            // ostbutton
            // 
            this.ostbutton.Location = new System.Drawing.Point(622, 102);
            this.ostbutton.Name = "ostbutton";
            this.ostbutton.Size = new System.Drawing.Size(75, 75);
            this.ostbutton.TabIndex = 2;
            this.ostbutton.Text = "Остановки";
            this.ostbutton.UseVisualStyleBackColor = true;
            this.ostbutton.Visible = false;
            this.ostbutton.Click += new System.EventHandler(this.ostbutton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(469, 450);
            this.Controls.Add(this.ostbutton);
            this.Controls.Add(this.trollbutton);
            this.Controls.Add(this.avtobutton);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button avtobutton;
        private System.Windows.Forms.Button trollbutton;
        private System.Windows.Forms.Button ostbutton;
    }
}

