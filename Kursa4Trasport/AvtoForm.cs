﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kursa4Trasport
{
    public partial class AvtoForm : Form
    {
        private string text;
        private string text1;
        public int number;
        private string num;
        DBconnect dBconnect = new DBconnect();
        public AvtoForm()
        {
            InitializeComponent();
        }

        private void AvtoForm_Load(object sender, EventArgs e)
        {
            dBconnect.ConnOpen();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            number = Convert.ToInt32(button1.Text);
            num = button1.Text;
            
            SqlCommand command = new SqlCommand("SELECT Marshr1 FROM Marshrutes WHERE ID = @id" ,dBconnect.SqlConnection());
            command.Parameters.Add ("@id", SqlDbType.VarChar).Value = number;
            SqlDataReader read = command.ExecuteReader();
            List<string> listIDs = new List<string>();

            read.Read();
             text = $"{read["Marshr1"]}";
            read.Close();

            SqlCommand command1 = new SqlCommand("SELECT Marsh2 FROM Marshrutes WHERE ID = @id", dBconnect.SqlConnection());
            command1.Parameters.Add("@id", SqlDbType.VarChar).Value = number;
            SqlDataReader read1 = command1.ExecuteReader();

            read1.Read();
            text1 = $"{read1["Marsh2"]}";
                    
            Choose choose = new Choose();
            choose.text1 = text1;
            choose.text = text;
            read.Close();
            Time time = new Time();
            time.num = num;

            this.Hide();
            choose.Show();           
            dBconnect.ConnClosed();
   

        }

      

        private void button2_Click(object sender, EventArgs e)
        {
            number = Convert.ToInt32(button2.Text);
            num = button2.Text;

            SqlCommand command = new SqlCommand("SELECT Marshr1 FROM Marshrutes WHERE ID = @id", dBconnect.SqlConnection());
            command.Parameters.Add("@id", SqlDbType.VarChar).Value = number;
            SqlDataReader read = command.ExecuteReader();
            List<string> listIDs = new List<string>();

            read.Read();
            text = $"{read["Marshr1"]}";
            read.Close();

            SqlCommand command1 = new SqlCommand("SELECT Marsh2 FROM Marshrutes WHERE ID = @id", dBconnect.SqlConnection());
            command1.Parameters.Add("@id", SqlDbType.VarChar).Value = number;
            SqlDataReader read1 = command1.ExecuteReader();

            read1.Read();
            text1 = $"{read1["Marsh2"]}";

            Choose choose = new Choose();
            choose.text1 = text1;
            choose.text = text;
            read.Close();
            Time time = new Time();
            time.num = num;

            this.Hide();
            choose.Show();
            dBconnect.ConnClosed();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            number = Convert.ToInt32(button3.Text);
            num = button3.Text;

            SqlCommand command = new SqlCommand("SELECT Marshr1 FROM Marshrutes WHERE ID = @id", dBconnect.SqlConnection());
            command.Parameters.Add("@id", SqlDbType.VarChar).Value = number;
            SqlDataReader read = command.ExecuteReader();
            List<string> listIDs = new List<string>();

            read.Read();
            text = $"{read["Marshr1"]}";
            read.Close();

            SqlCommand command1 = new SqlCommand("SELECT Marsh2 FROM Marshrutes WHERE ID = @id", dBconnect.SqlConnection());
            command1.Parameters.Add("@id", SqlDbType.VarChar).Value = number;
            SqlDataReader read1 = command1.ExecuteReader();

            read1.Read();
            text1 = $"{read1["Marsh2"]}";

            Choose choose = new Choose();
            choose.text1 = text1;
            choose.text = text;
            read.Close();
            Time time = new Time();
            time.num = num;

            this.Hide();
            choose.Show();
            dBconnect.ConnClosed();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            number = Convert.ToInt32(button4.Text);
            num = button4.Text;

            SqlCommand command = new SqlCommand("SELECT Marshr1 FROM Marshrutes WHERE ID = @id", dBconnect.SqlConnection());
            command.Parameters.Add("@id", SqlDbType.VarChar).Value = number;
            SqlDataReader read = command.ExecuteReader();
            List<string> listIDs = new List<string>();

            read.Read();
            text = $"{read["Marshr1"]}";
            read.Close();

            SqlCommand command1 = new SqlCommand("SELECT Marsh2 FROM Marshrutes WHERE ID = @id", dBconnect.SqlConnection());
            command1.Parameters.Add("@id", SqlDbType.VarChar).Value = number;
            SqlDataReader read1 = command1.ExecuteReader();

            read1.Read();
            text1 = $"{read1["Marsh2"]}";

            Choose choose = new Choose();
            choose.text1 = text1;
            choose.text = text;
            read.Close();
            Time time = new Time();
            time.num = num;

            this.Hide();
            choose.Show();
            dBconnect.ConnClosed();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            number = Convert.ToInt32(button5.Text);
            num = button5.Text;

            SqlCommand command = new SqlCommand("SELECT Marshr1 FROM Marshrutes WHERE ID = @id", dBconnect.SqlConnection());
            command.Parameters.Add("@id", SqlDbType.VarChar).Value = number;
            SqlDataReader read = command.ExecuteReader();
            List<string> listIDs = new List<string>();

            read.Read();
            text = $"{read["Marshr1"]}";
            read.Close();

            SqlCommand command1 = new SqlCommand("SELECT Marsh2 FROM Marshrutes WHERE ID = @id", dBconnect.SqlConnection());
            command1.Parameters.Add("@id", SqlDbType.VarChar).Value = number;
            SqlDataReader read1 = command1.ExecuteReader();

            read1.Read();
            text1 = $"{read1["Marsh2"]}";

            Choose choose = new Choose();
            choose.text1 = text1;
            choose.text = text;
            read.Close();
            Time time = new Time();
            time.num = num;

            this.Hide();
            choose.Show();
            dBconnect.ConnClosed();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            number = Convert.ToInt32(button6.Text);
            num = button6.Text;

            SqlCommand command = new SqlCommand("SELECT Marshr1 FROM Marshrutes WHERE ID = @id", dBconnect.SqlConnection());
            command.Parameters.Add("@id", SqlDbType.VarChar).Value = number;
            SqlDataReader read = command.ExecuteReader();
            List<string> listIDs = new List<string>();

            read.Read();
            text = $"{read["Marshr1"]}";
            read.Close();

            SqlCommand command1 = new SqlCommand("SELECT Marsh2 FROM Marshrutes WHERE ID = @id", dBconnect.SqlConnection());
            command1.Parameters.Add("@id", SqlDbType.VarChar).Value = number;
            SqlDataReader read1 = command1.ExecuteReader();

            read1.Read();
            text1 = $"{read1["Marsh2"]}";

            Choose choose = new Choose();
            choose.text1 = text1;
            choose.text = text;
            read.Close();
            Time time = new Time();
            time.num = num;

            this.Hide();
            choose.Show();
            dBconnect.ConnClosed();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            number = Convert.ToInt32(button7.Text);
            num = button7.Text;

            SqlCommand command = new SqlCommand("SELECT Marshr1 FROM Marshrutes WHERE ID = @id", dBconnect.SqlConnection());
            command.Parameters.Add("@id", SqlDbType.VarChar).Value = number;
            SqlDataReader read = command.ExecuteReader();
            List<string> listIDs = new List<string>();

            read.Read();
            text = $"{read["Marshr1"]}";
            read.Close();

            SqlCommand command1 = new SqlCommand("SELECT Marsh2 FROM Marshrutes WHERE ID = @id", dBconnect.SqlConnection());
            command1.Parameters.Add("@id", SqlDbType.VarChar).Value = number;
            SqlDataReader read1 = command1.ExecuteReader();

            read1.Read();
            text1 = $"{read1["Marsh2"]}";

            Choose choose = new Choose();
            choose.text1 = text1;
            choose.text = text;
            read.Close();
            Time time = new Time();
            time.num = num;

            this.Hide();
            choose.Show();
            dBconnect.ConnClosed();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            number = Convert.ToInt32(button8.Text);
            num = button8.Text;

            SqlCommand command = new SqlCommand("SELECT Marshr1 FROM Marshrutes WHERE ID = @id", dBconnect.SqlConnection());
            command.Parameters.Add("@id", SqlDbType.VarChar).Value = number;
            SqlDataReader read = command.ExecuteReader();
            List<string> listIDs = new List<string>();

            read.Read();
            text = $"{read["Marshr1"]}";
            read.Close();

            SqlCommand command1 = new SqlCommand("SELECT Marsh2 FROM Marshrutes WHERE ID = @id", dBconnect.SqlConnection());
            command1.Parameters.Add("@id", SqlDbType.VarChar).Value = number;
            SqlDataReader read1 = command1.ExecuteReader();

            read1.Read();
            text1 = $"{read1["Marsh2"]}";

            Choose choose = new Choose();
            choose.text1 = text1;
            choose.text = text;
            read.Close();
            Time time = new Time();
            time.num = num;

            this.Hide();
            choose.Show();
            dBconnect.ConnClosed();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            number = Convert.ToInt32(button9.Text);
            num = button9.Text;

            SqlCommand command = new SqlCommand("SELECT Marshr1 FROM Marshrutes WHERE ID = @id", dBconnect.SqlConnection());
            command.Parameters.Add("@id", SqlDbType.VarChar).Value = number;
            SqlDataReader read = command.ExecuteReader();
            List<string> listIDs = new List<string>();

            read.Read();
            text = $"{read["Marshr1"]}";
            read.Close();

            SqlCommand command1 = new SqlCommand("SELECT Marsh2 FROM Marshrutes WHERE ID = @id", dBconnect.SqlConnection());
            command1.Parameters.Add("@id", SqlDbType.VarChar).Value = number;
            SqlDataReader read1 = command1.ExecuteReader();

            read1.Read();
            text1 = $"{read1["Marsh2"]}";

            Choose choose = new Choose();
            choose.text1 = text1;
            choose.text = text;
            read.Close();
            Time time = new Time();
            time.num = num;

            this.Hide();
            choose.Show();
            dBconnect.ConnClosed();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            number = Convert.ToInt32(button10.Text);
            num = button10.Text;

            SqlCommand command = new SqlCommand("SELECT Marshr1 FROM Marshrutes WHERE ID = @id", dBconnect.SqlConnection());
            command.Parameters.Add("@id", SqlDbType.VarChar).Value = number;
            SqlDataReader read = command.ExecuteReader();
            List<string> listIDs = new List<string>();

            read.Read();
            text = $"{read["Marshr1"]}";
            read.Close();

            SqlCommand command1 = new SqlCommand("SELECT Marsh2 FROM Marshrutes WHERE ID = @id", dBconnect.SqlConnection());
            command1.Parameters.Add("@id", SqlDbType.VarChar).Value = number;
            SqlDataReader read1 = command1.ExecuteReader();

            read1.Read();
            text1 = $"{read1["Marsh2"]}";

            Choose choose = new Choose();
            choose.text1 = text1;
            choose.text = text;
            read.Close();
            Time time = new Time();
            time.num = num;

            this.Hide();
            choose.Show();
            dBconnect.ConnClosed();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            number = Convert.ToInt32(button11.Text);
            num = button11.Text;

            SqlCommand command = new SqlCommand("SELECT Marshr1 FROM Marshrutes WHERE ID = @id", dBconnect.SqlConnection());
            command.Parameters.Add("@id", SqlDbType.VarChar).Value = number;
            SqlDataReader read = command.ExecuteReader();
            List<string> listIDs = new List<string>();

            read.Read();
            text = $"{read["Marshr1"]}";
            read.Close();

            SqlCommand command1 = new SqlCommand("SELECT Marsh2 FROM Marshrutes WHERE ID = @id", dBconnect.SqlConnection());
            command1.Parameters.Add("@id", SqlDbType.VarChar).Value = number;
            SqlDataReader read1 = command1.ExecuteReader();

            read1.Read();
            text1 = $"{read1["Marsh2"]}";

            Choose choose = new Choose();
            choose.text1 = text1;
            choose.text = text;
            read.Close();
            Time time = new Time();
            time.num = num;

            this.Hide();
            choose.Show();
            dBconnect.ConnClosed();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            number = Convert.ToInt32(button12.Text);
            num = button12.Text;

            SqlCommand command = new SqlCommand("SELECT Marshr1 FROM Marshrutes WHERE ID = @id", dBconnect.SqlConnection());
            command.Parameters.Add("@id", SqlDbType.VarChar).Value = number;
            SqlDataReader read = command.ExecuteReader();
            List<string> listIDs = new List<string>();

            read.Read();
            text = $"{read["Marshr1"]}";
            read.Close();

            SqlCommand command1 = new SqlCommand("SELECT Marsh2 FROM Marshrutes WHERE ID = @id", dBconnect.SqlConnection());
            command1.Parameters.Add("@id", SqlDbType.VarChar).Value = number;
            SqlDataReader read1 = command1.ExecuteReader();

            read1.Read();
            text1 = $"{read1["Marsh2"]}";

            Choose choose = new Choose();
            choose.text1 = text1;
            choose.text = text;
            read.Close();
            Time time = new Time();
            time.num = num;

            this.Hide();
            choose.Show();
            dBconnect.ConnClosed();
        }

        private void button13_Click(object sender, EventArgs e)
        {
            number = Convert.ToInt32(button13.Text);
            num = button13.Text;

            SqlCommand command = new SqlCommand("SELECT Marshr1 FROM Marshrutes WHERE ID = @id", dBconnect.SqlConnection());
            command.Parameters.Add("@id", SqlDbType.VarChar).Value = number;
            SqlDataReader read = command.ExecuteReader();
            List<string> listIDs = new List<string>();

            read.Read();
            text = $"{read["Marshr1"]}";
            read.Close();

            SqlCommand command1 = new SqlCommand("SELECT Marsh2 FROM Marshrutes WHERE ID = @id", dBconnect.SqlConnection());
            command1.Parameters.Add("@id", SqlDbType.VarChar).Value = number;
            SqlDataReader read1 = command1.ExecuteReader();

            read1.Read();
            text1 = $"{read1["Marsh2"]}";

            Choose choose = new Choose();
            choose.text1 = text1;
            choose.text = text;
            read.Close();
            Time time = new Time();
            time.num = num;

            this.Hide();
            choose.Show();
            dBconnect.ConnClosed();
        }

        private void button14_Click(object sender, EventArgs e)
        {
            number = Convert.ToInt32(button14.Text);
            num = button14.Text;

            SqlCommand command = new SqlCommand("SELECT Marshr1 FROM Marshrutes WHERE ID = @id", dBconnect.SqlConnection());
            command.Parameters.Add("@id", SqlDbType.VarChar).Value = number;
            SqlDataReader read = command.ExecuteReader();
            List<string> listIDs = new List<string>();

            read.Read();
            text = $"{read["Marshr1"]}";
            read.Close();

            SqlCommand command1 = new SqlCommand("SELECT Marsh2 FROM Marshrutes WHERE ID = @id", dBconnect.SqlConnection());
            command1.Parameters.Add("@id", SqlDbType.VarChar).Value = number;
            SqlDataReader read1 = command1.ExecuteReader();

            read1.Read();
            text1 = $"{read1["Marsh2"]}";

            Choose choose = new Choose();
            choose.text1 = text1;
            choose.text = text;
            read.Close();
            Time time = new Time();
            time.num = num;

            this.Hide();
            choose.Show();
            dBconnect.ConnClosed();
        }

        private void button15_Click(object sender, EventArgs e)
        {
            number = Convert.ToInt32(button15.Text);
            num = button15.Text;

            SqlCommand command = new SqlCommand("SELECT Marshr1 FROM Marshrutes WHERE ID = @id", dBconnect.SqlConnection());
            command.Parameters.Add("@id", SqlDbType.VarChar).Value = number;
            SqlDataReader read = command.ExecuteReader();
            List<string> listIDs = new List<string>();

            read.Read();
            text = $"{read["Marshr1"]}";
            read.Close();

            SqlCommand command1 = new SqlCommand("SELECT Marsh2 FROM Marshrutes WHERE ID = @id", dBconnect.SqlConnection());
            command1.Parameters.Add("@id", SqlDbType.VarChar).Value = number;
            SqlDataReader read1 = command1.ExecuteReader();

            read1.Read();
            text1 = $"{read1["Marsh2"]}";

            Choose choose = new Choose();
            choose.text1 = text1;
            choose.text = text;
            read.Close();
            Time time = new Time();
            time.num = num;

            this.Hide();
            choose.Show();
            dBconnect.ConnClosed();
        }

        private void button16_Click(object sender, EventArgs e)
        {
            number = Convert.ToInt32(button16.Text);
            num = button16.Text;

            SqlCommand command = new SqlCommand("SELECT Marshr1 FROM Marshrutes WHERE ID = @id", dBconnect.SqlConnection());
            command.Parameters.Add("@id", SqlDbType.VarChar).Value = number;
            SqlDataReader read = command.ExecuteReader();
            List<string> listIDs = new List<string>();

            read.Read();
            text = $"{read["Marshr1"]}";
            read.Close();

            SqlCommand command1 = new SqlCommand("SELECT Marsh2 FROM Marshrutes WHERE ID = @id", dBconnect.SqlConnection());
            command1.Parameters.Add("@id", SqlDbType.VarChar).Value = number;
            SqlDataReader read1 = command1.ExecuteReader();

            read1.Read();
            text1 = $"{read1["Marsh2"]}";

            Choose choose = new Choose();
            choose.text1 = text1;
            choose.text = text;
            read.Close();
            Time time = new Time();
            time.num = num;

            this.Hide();
            choose.Show();
            dBconnect.ConnClosed();
        }
        public string TEXT()
        {
            return this.text;
        }
        public string TEXT1()
        {
            return this.text1;
        }

        private void helpB_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Добро пожаловать в приложение \" Расписание автобусов\"\n Вы нажали на меню помощи, здесь вкратце будет объясненно как работа" +
                "ет это приложение" +
                "\n Перед вами есть кнопки, каждая кнопка отвечает за конкретный автобус, нажав на кнопку вас перекинет в следующее меню, где будут показан маршрут этого авобуса и остановки, на которых он останавливается" +
                ".\n Вам необходимо выбрать нужную вам остановку и нажать на кнопку \" Выбрать \", после этого откроется рассписание прибытия автобуса на даунню конкретную остановку." +
                "\n Удачного пользования приложением!");
        }
    }
}
